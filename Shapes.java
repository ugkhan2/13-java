package shapes;

import java.awt.Color;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class Shapes extends Frame implements KeyListener{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -394346513878033111L;
	String name = "";
	int num = 0;
	int[] windowSize = new int[2];
	int typeofShape = 0;
	int[] center = new int[2];
	int length = 0;
	String[] coordinates = new String[100];
	
	int btnUP = 0;
	int btnDOWN = 0;
	int btnLEFT = 0;
	int btnRIGHT = 0;
	int keycode = 0;
	
	int count = 0;
	
	FileReader fRead = new FileReader("/home/khan/DEV/13-java/exercise.txt");
	BufferedReader bRead = new BufferedReader(fRead);
	
	
	
	Shapes() throws IOException {
		String tmp = "";
		tmp = bRead.readLine();
		windowSize = stoa(tmp);		
		name = bRead.readLine();
		initWindow(name, windowSize);
		typeofShape = stoi(bRead.readLine());
		tmp = bRead.readLine();
		center = stoa(tmp);
		length = stoi(bRead.readLine());
		
		String currentLine;
		
		while((currentLine = bRead.readLine()) != null) {
			coordinates[count] = currentLine;
			count++;
		}
	}
	
	public void initWindow(String title, int[] size) {
		setTitle(title);
		setSize(size[0], size[1]);
		setBackground(Color.GRAY);
		
		addWindowListener(new WindowClosingAdapter(true));
		addKeyListener(this);
		setVisible(true);
	}
	
	public void paint(Graphics g) {
		
		g.drawString("UP : " + btnUP, 150, 150);
		
		g.setColor(Color.white);
		switch (typeofShape) {
		case 1:
			for(int i = 0; i < coordinates.length; i += 1) {
				int[] tmpCoordinates = stoa(coordinates[i]);
				g.fillRect(tmpCoordinates[0], tmpCoordinates[1], 10, 20);
			}
			break;
			
		case 2:
			g.fillOval(100, 100, 10, 10);
			break;

		default:
			break;
		}
	}
	
	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void keyPressed(KeyEvent e) {
		
		keycode = e.getKeyCode();
		
		keycode = KeyEvent.VK_UP;
		
		if(e.getKeyCode() == 38) {
			for(int i = 0; i < coordinates.length; i += 1) {
				int[] currentNum = stoa(coordinates[i]);
				coordinates[i] = String.valueOf(currentNum[0]) + " " + String.valueOf(currentNum[1]-10);
			}
						
			btnUP++;
		}
		
		
		// TODO Auto-generated method stub
//		
//		if(e.getKeyCode()==KeyEvent.VK_S) y=y+10;
//		if(e.getKeyCode()==KeyEvent.VK_A) x=x-10;
//		if(e.getKeyCode()==KeyEvent.VK_D) x=x+10;
		repaint();
		
	}
	
	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}
	
	public int[] stoa(String chunk) {
		int[] arr = new int[2];
		int i = 0;
		for(String s: chunk.split(" ")) {			
			arr[i] = stoi(s);
			i++;
		}
		
		return arr;
	}
	
	public int stoi(String a) {
		return Integer.parseInt(a);
	}
	
	
	
	public static void main(String[] args) throws IOException{
		Shapes myShapes = new Shapes();
		System.out.println(myShapes.typeofShape);;
	}
	

}
